# README #

CourseCase: a case for writing a JSP framework.

### What is this repository for? ###

* A Java web application using Java SE 8 and Java EE 7, but without a standard server-side framework. Instead, it contains its own custom framework: JS*fred* (Java Server *fr*amework for *ed*ucational purposes).
* Version 0.3

### Features? ###

* JSP tag library
* Page templating
* Front controller
* Backing beans for custom conversion/validation and executing actions
* JPA entity classes
* Bean validation
* RESTful web services

### How do I get set up? ###

* Install NetBeans with Glassfish 4.1 (not 4.1.1)
* Start JavaDB
* Create database "CourseCase" (user "fred", password "fred")
* Open this project
* Run

### Contribution guidelines ###

* Ask Fred

### Who do I talk to? ###

* Fred Zuijdendorp (fred.zuijdendorp@mac.com)