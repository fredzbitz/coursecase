/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import jsfred.controller.Parameters;

/**
 *
 * @author fred
 */
@Entity
@XmlRootElement
public class Teacher extends Person {

	@OneToMany(mappedBy = "teacher")
	private List<Course> courses;

	public Teacher() {
	}

	public Teacher(String firstName, String lastName, Address address, Date birthDate) {
		super(firstName, lastName, address, birthDate);
	}

	@XmlTransient
	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}
	
	public void setFields(Parameters parameters) {
		super.setFields(parameters);
	}

}
