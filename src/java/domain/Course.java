/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.ArrayList;
import jsfred.model.Record;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import jsfred.constraints.Required;
import jsfred.controller.Parameters;

/**
 *
 * @author fred
 */
@Entity
@XmlRootElement
public class Course extends Record {
	@Required private String title;
	@ManyToOne private Teacher teacher;
	@ManyToMany private List<Student> students;
	@NotNull(message = "FIELD_REQUIRED") @Temporal(TemporalType.DATE) private Date startDate;
	@Min(1) private int durationDays;

	public Course() {
	}
	
	public Course(String title, Teacher teacher, Date startDate, int durationDays) {
		this.title = title;
		this.teacher = teacher;
		this.students = new ArrayList<>();
		this.startDate = startDate;
		this.durationDays = durationDays;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

//	@XmlTransient
	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}
	
	public void addStudent(Student student) {
		students.add(student);
	}

	public void deleteStudent(Student student) {
		students.remove(student);
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public int getDurationDays() {
		return durationDays;
	}

	public void setDurationDays(int durationDays) {
		this.durationDays = durationDays;
	}

	public void setFields(Parameters parameters) {
		super.setFields(parameters);
		setTitle(parameters.getString("title"));
		setTeacher((Teacher) parameters.get("teacher"));
		setStartDate((Date) parameters.get("startDate"));
		Integer integer = (Integer) parameters.get("durationDays");
		setDurationDays(integer != null ? integer : 0);
	}

	@Override
	public String toString() {
		return String.format("%s (%s), %s", title, teacher, startDate);
	}
}
