/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import constraints.ValidZip;
import jsfred.model.Record;
import jsfred.controller.Parameters;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;
import jsfred.constraints.Required;

/**
 *
 * @author fred
 */
@Entity
@XmlRootElement
@ValidZip
public class Address extends Record {
	
	@Required
	private String street;
	@Required
	private String number;
	@Required
	private String zip;
	@Required
	private String city;
	@Required
	private String country;

	public Address() {
	}
	
	public Address(String street, String number, String zip, String city, String country) {
		setStreet(street);
		setNumber(number);
		setZip(zip);
		setCity(city);
		setCountry(country);
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setFields(Parameters parameters) {
		super.setFields(parameters);
		setStreet(parameters.getString("street"));
		setNumber(parameters.getString("number"));
		setZip(parameters.getString("zip"));
		setCity(parameters.getString("city"));
		setCountry(parameters.getString("country"));
	}

	@Override
	public String toString() {
		return String.format("%s %s, %s, %s", street, number, city, country);
	}
	
}
