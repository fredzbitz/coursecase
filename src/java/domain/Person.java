/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import jsfred.model.Record;
import java.util.Date;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;
import jsfred.constraints.Required;
import jsfred.controller.Parameters;

/**
 *
 * @author fred
 */
@MappedSuperclass
public class Person extends Record {

	@Required private String firstName;
	@Required private String lastName;
	@OneToOne private Address address;
	@NotNull(message = "FIELD_REQUIRED") @Temporal(javax.persistence.TemporalType.DATE) private Date birthDate;
	
	public Person() {
	}

	public Person(String firstName, String lastName, Address address, Date birthDate) {
		setFirstName(firstName);
		setLastName(lastName);
		setAddress(address);
		setBirthDate(birthDate);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public void setFields(Parameters parameters) {
		super.setFields(parameters);
		setFirstName(parameters.getString("firstName"));
		setLastName(parameters.getString("lastName"));
		setAddress((Address) parameters.get("address"));
		setBirthDate((Date) parameters.get("birthDate"));
	}

	@Override
	public String toString() {
		return String.format("%s %s", firstName, lastName);
	}
	
}
