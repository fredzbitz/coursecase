/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import jsfred.controller.Parameters;

/**
 *
 * @author fred
 */
@Entity
@XmlRootElement
public class Student extends Person {

	@ManyToMany(mappedBy = "students")
	private List<Course> courses;

	public Student() {
	}

	public Student(String firstName, String lastName, Address address, Date birthDate) {
		super(firstName, lastName, address, birthDate);
	}

	@XmlTransient
	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}
	
	public void setFields(Parameters parameters) {
		super.setFields(parameters);
	}

}
