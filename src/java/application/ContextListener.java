/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Web application lifecycle listener.
 *
 * @author fred
 */
public class ContextListener implements ServletContextListener {
	
	private static String database;

	private static final Logger LOG = Logger.getLogger(ContextListener.class.getName());

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		String user = sce.getServletContext().getInitParameter("user");
		LOG.info("--- CourseCase starting for user " + user);
		database = sce.getServletContext().getInitParameter("database");
		if (database == null) database = "";
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		LOG.info("--- CourseCase stopping");
	}

	// return the PU name based on the "database" context parameter
	// was supposed to be used in @PersistenceContext annotations, but they allow only compile-time values
	public static String getPuName() {
		return "CourseCasePU" + database;
	}
}
