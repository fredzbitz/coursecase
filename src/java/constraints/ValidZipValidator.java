/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constraints;

import domain.Address;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author fred
 */
public class ValidZipValidator implements ConstraintValidator<ValidZip, Address> {

	final private Pattern NL_ZIP = Pattern.compile("^\\d{4} ?\\p{Alpha}{2}$");
	final private Pattern BE_ZIP = Pattern.compile("^\\d{4}$");
	private Map<String, Pattern> zipPatterns;

	@Override
	public void initialize(ValidZip constraintAnnotation) {
		zipPatterns = new HashMap<>();
		zipPatterns.put("nederland", NL_ZIP);
		zipPatterns.put("netherlands", NL_ZIP);
		zipPatterns.put("holland", NL_ZIP);
		zipPatterns.put("nl", NL_ZIP);
		zipPatterns.put("belgie", BE_ZIP);
		zipPatterns.put("belgium", BE_ZIP);
		zipPatterns.put("be", BE_ZIP);
	}

	@Override
	public boolean isValid(Address address, ConstraintValidatorContext context) {
		System.out.println("=== validating: " + address);
		if (address == null) {
			return true;
		}
		String country = address.getCountry();
		String zip = address.getZip();
		if (country != null && zip != null) {
			Pattern pattern = zipPatterns.get(country.toLowerCase());
			if (pattern != null && !pattern.matcher(zip).matches()) {
				return false;
			}
		}
		return true;
	}
}
