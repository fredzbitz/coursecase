/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import domain.Address;
import java.util.List;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author fred
 */
public class CourseCase {
	public static void main(String[] args) {
         AddressClient client = new AddressClient();
         String response = client.findAll_XML(String.class);
		 System.out.println("response: " + response);
         List<Address> addresses = client.findAll();
		 System.out.println("addresses: " + addresses);
         client.close();
	}
}
