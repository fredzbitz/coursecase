/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import jsfred.controller.AbstractBackingBean;
import jsfred.controller.Messages;
import domain.Address;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import model.AddressFacade;

/**
 *
 * @author fred
 */
@Named
@SessionScoped
public class AddressBean extends AbstractBackingBean<Address> {

	@EJB
	private AddressFacade facade;

	public AddressBean() {
//		System.out.println("..+ AddressBean created");
	}

	@Override
	public AddressFacade getCourseFacade() {
		return facade;
	}

	public Address newRecord() {
		return new Address();
	}

	@Override
	public void convert(Messages messages) throws ConversionException {
		// for each address property that is not a String, replace the corresponding value in the parameters
		// however... they're all Strings, so we're done
	}

	@Override
	public void validate(Messages messages) throws ValidationException {
		// general validation (including bean validation)
		super.validate(messages);
		
		// specific validation
		// now handled by bean validation
//		require("street", messages);
//		require("number", messages);
//		require("zip", messages);
//		require("city", messages);
//		require("country", messages);

		// throw exception if any validation errors occurred
		if (messages.size() != 0) {
			throw new ValidationException(messages, getAction());
		}
	}
}
