/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Address;
import domain.Course;
import domain.Student;
import domain.Teacher;
import java.util.Date;
import javax.ejb.EJB;
import jsfred.controller.AbstractBackingBean;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import jsfred.controller.Messages;
import jsfred.model.AbstractFacade;
import jsfred.model.Record;
import static jsfred.view.ActionType.*;
import model.AddressFacade;
import model.CourseFacade;
import model.StudentFacade;
import model.TeacherFacade;

/**
 *
 * @author fred
 */
@Named
@SessionScoped
public class HomeBean extends AbstractBackingBean<Record> {

	@EJB
	private AddressFacade addressFacade;
	@EJB
	private StudentFacade studentFacade;
	@EJB
	private TeacherFacade teacherFacade;
	@EJB
	private CourseFacade courseFacade;

	
	public HomeBean() {
//		System.out.println("..+ HomeBean created");
	}

	
	@Override
	public void convert(Messages messages) {
	}

	
	@Override
	public void validate(Messages messages) {
	}

	
	@Override
	public String cancel() {
		return DEFAULT.pageForEntity(getEntity());
	}

	
	@Override
	public AbstractFacade getCourseFacade() {
		return null;
	}

	
	public Record newRecord() {
		return new Record();
	}

	
	@Override
	protected String action(String action, Messages messages) {
		switch (getAction()) {
			case "init":
				initDatabase();
				return DEFAULT.pageForEntity(getEntity());
		}
		return null;
	}

	
	private void initDatabase() {
		Course course;
		Teacher teacher;
		Student student;
		Address address;

		address = new Address("Biltstraat", "16", "3578 GH", "Utrecht", "Nederland");
		addressFacade.create(address);
		teacher = new Teacher("Albert", "Schweitzer", address, new Date(0001, 7, 8));
		teacherFacade.create(teacher);
		course = new Course("Relatively Speaking", teacher, new Date(0117, 1, 1), 5);
		courseFacade.create(course);

		address = new Address("Wilhelminapark", "12", "3564 GF", "Utrecht", "Nederland");
		addressFacade.create(address);
		student = new Student("Reinhard", "Wippler", address, new Date(0030, 10, 23));
		studentFacade.create(student);
		course.addStudent(student);
		courseFacade.edit(course);

		address = new Address("Nachtegaalstraat", "32", "3563 AG", "Utrecht", "Nederland");
		addressFacade.create(address);
		student = new Student("Maria", "Popova", address, new Date(0067, 4, 16));
		studentFacade.create(student);
		course.addStudent(student);
		courseFacade.edit(course);
	}

}
