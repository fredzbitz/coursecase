/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import domain.Course;
import domain.Student;
import jsfred.controller.AbstractBackingBean;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import jsfred.controller.Messages;
import static jsfred.view.ActionType.*;
import model.CourseFacade;
import model.StudentFacade;
import model.TeacherFacade;

/**
 *
 * @author fred
 */
@Named
@SessionScoped
public class CourseBean extends AbstractBackingBean<Course> {

	@EJB
	private CourseFacade courseFacade;
	@EJB
	private TeacherFacade teacherFacade;
	@EJB
	private StudentFacade studentFacade;
	
	
	public CourseBean() {
//		System.out.println("..+ CourseBean created");
	}
	
	
	@Override
	public CourseFacade getCourseFacade() {
		return courseFacade;
	}

	
	public Course newRecord() {
		return new Course();
	}

	
	@Override
	public void convert(Messages messages) throws ConversionException {
		convertToDate("startDate", messages);
		convertToRecord("teacher", teacherFacade, messages);
		convertToInteger("durationDays", messages);
		convertToLong("ref_id", messages);

		// throw exception if any conversion errors occurred
		if (messages.size() != 0) {
			throw new ConversionException(messages, getAction());
		}
	}

	
	@Override
	public void validate(Messages messages) throws ValidationException {
		// general validation (including bean validation)
		super.validate(messages);
		
		// specific validation
		// ...

		// throw exception if any validation errors occurred
		if (messages.size() != 0) {
			throw new ValidationException(messages, getAction());
		}
	}

	
	@Override
	protected String action(String action, Messages messages) {
		switch (action) {
			case "delete_student": {
				System.out.println("=== deleting student");
//				// TEST error handling
//				messages.add("delete_student", "could not delete student");
				// get student
				Long studentId = getParameters().getLong("ref_id");
				Student student = studentFacade.find(studentId);
				System.out.println("... deleting student: " + student);
				// deleting from course
				Course course = getSelectedRecord();
				course.deleteStudent(student);
				courseFacade.edit(course);
				return SHOW.pageForEntity(getEntity());
			}
			case "add_student": {
				System.out.println("=== adding student");
//				// TEST error handling
//				messages.add("add_student", "could not add student");
				// get student
				Long studentId = getParameters().getLong("ref_id");
				Student student = studentFacade.find(studentId);
				System.out.println("... adding student: " + student);
				// add to course
				Course course = getSelectedRecord();
				course.addStudent(student);
				courseFacade.edit(course);
				return SHOW.pageForEntity(getEntity());
			}
		}
		return null;
	}

	
	@Override	// until JSP recognizes the default method inherited from the BackingBean interface
	public List<Course> getRecords() {
		return super.getRecords();
	}
	
	
	public List<Student> getAvailableStudents() {
		List<Student> students = studentFacade.findAll();
		Course course = getSelectedRecord();
		students.removeIf(student -> course.getStudents().contains(student));
		return students;
	}
	
	
	public void addStudent(Student student) {
		Course course = getSelectedRecord();
		course.addStudent(student);
		courseFacade.edit(course);
	}
}
