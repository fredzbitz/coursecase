/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import jsfred.controller.AbstractBackingBean;
import domain.Teacher;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import jsfred.controller.Messages;
import static jsfred.view.ActionType.LIST;
import model.AddressFacade;
import model.TeacherFacade;

/**
 *
 * @author fred
 */
@Named
@SessionScoped
public class TeacherBean extends AbstractBackingBean<Teacher> {

	@EJB
	private TeacherFacade facade;
	@EJB
	private AddressFacade addressFacade;
	
	public TeacherBean() {
//		System.out.println("..+ TeacherBean created");
	}
	
	@Override
	public void convert(Messages messages) throws ConversionException {
		convertToDate("birthDate", messages);
		convertToRecord("address", addressFacade, messages);

		// throw exception if any conversion errors occurred
		if (messages.size() != 0) {
			throw new ConversionException(messages, getAction());
		}
	}

	@Override
	public void validate(Messages messages) throws ValidationException {
		// general validation (including bean validation)
		super.validate(messages);
		
		// specific validation
		// ...

		// throw exception if any validation errors occurred
		if (messages.size() != 0) {
			throw new ValidationException(messages, getAction());
		}
	}

	@Override
	public TeacherFacade getCourseFacade() {
		return facade;
	}

	public Teacher newRecord() {
		return new Teacher();
	}

	@Override	// until JSP recognizes the default method inherited from the BackingBean interface
	public List<Teacher> getRecords() {
		return super.getRecords();
	}
	
}
