/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfred.controller;

import java.io.IOException;
import javax.ejb.EJBException;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author fred
 */
@WebServlet(name = "PageController", urlPatterns = {"/page/*"})
public class PageController extends HttpServlet {

	@Inject
	@DynamicBean
	@RequestScoped
	BackingBean backingBean;

	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request servlet request, is used by bla when bla if bla hahaha
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String path = request.getPathInfo();
		String queryString = request.getQueryString();
		System.out.printf("--> GET: %s%s%n", path, queryString != null ? "?" + queryString : "");
		if (path.endsWith("/")) {
			path = path.substring(0, path.length() - 1);
		}

		// backing bean will extract parameters and query from request:
		backingBean.setRequest(request);

		// dispatch
		String result = String.format("/WEB-INF/pages%s.jsp", path);

		// return page
		System.out.printf("<-- result: %s%n", result);
		RequestDispatcher dsp = request.getRequestDispatcher(result);
		dsp.forward(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String path = request.getPathInfo();
		System.out.printf("--> POST: %s%n", path);
		if (path.endsWith("/")) {
			path = path.substring(0, path.length() - 1);
		}
		String returnPath = path;

		// backing bean will extract parameters and query from request:
		backingBean.setRequest(request);
		String from = backingBean.getParameters().getString("from");
		if (from != null) {
			returnPath = String.format("/%s/%s", backingBean.getEntity(), from);
		}

		// was the request posted by a Cancel button?
		boolean cancelRequested = request.getParameter("cancel") != null;

		// use backing bean
		if (backingBean != null) {
			if (!cancelRequested) {
				try {
					// collect error messages
					Messages messages = new Messages();
					// convert request parameters:
					backingBean.convert(messages);
					// validate request parameters:
					backingBean.validate(messages);
					// execute ACTION:
					returnPath = backingBean.execute(messages);
				}
				catch (BackingBean.Exception bbx) {
					System.out.println("!!! " + bbx);
					Messages messages = bbx.getMessages();
					// pass the error messages to the page
					request.setAttribute("messages", messages);
					// return original page
//					returnPath = path;
				}
			}
			else {
				System.out.println("... cancel");
				returnPath = backingBean.cancel();
			}
		}

		// return page
		String result = String.format("/WEB-INF/pages%s.jsp", returnPath);
		System.out.printf("<-- result: %s%n", result);
		RequestDispatcher dsp = request.getRequestDispatcher(result);
		dsp.forward(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Front-end Page Controller";
	}

}
