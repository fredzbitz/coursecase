/*
 * Contains a producer method for dynamically creating BackingBeans that will be injected.
 * It is essential that the bean is RequestScoped, to force CDI to ask for a new bean with every request.
 * However, to allow for passing bean information from one request to another that has the same entity,
 * the producer will cache the bean and and re-use it if the entity is the same.
 * Also: note that we should NEVER create the bean object ourselves.
 * This would prevent injecion into the bean of for instance the Façade.
 * Also, it sould not store the bean into the (request) scope, maning the EL would not find it.
 */
package jsfred.controller;

import controller.AddressBean;
import controller.CourseBean;
import controller.HomeBean;
import controller.StudentBean;
import controller.TeacherBean;
import domain.Address;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.servlet.http.HttpServletRequest;
import jsfred.model.Record;

/**
 *
 * @author fred
 */
@SessionScoped
public class BeanProducer implements Serializable {

	private BackingBean currentBean;
	private String currentEntity;
	
	public BeanProducer() {
//		System.out.println("..+ BeanProducer created");
	}

	// inject all possible beans to make sure they are created by CDI
	// (otherwise they're not managed beans)
	// CDI will create these beans only once
	@Produces
	@DynamicBean
	@RequestScoped
	public BackingBean getBean(HttpServletRequest request,
			HomeBean homeBean,
			AddressBean addressBean,
			StudentBean studentBean,
			TeacherBean teacherBean,
			CourseBean courseBean
	) {
		String path = request.getPathInfo();
//		System.out.println("... getBean for request: " + path);
		
		// extract entity and action from path, which has the format: /ENTITY/ACTION?QUERY
		if (path.endsWith("/")) {
			path = path.substring(0, path.length() - 1);
		}
		int entityIndex = path.indexOf('/') + 1;
		if (entityIndex < 0) {
			entityIndex = 0;
		}
		int actionIndex = path.indexOf('/', entityIndex);
		if (actionIndex < 0) {
			actionIndex = path.length() - 1;
		}
		String entity = path.substring(entityIndex, actionIndex);
		String action = path.substring(actionIndex + 1, path.length());
		if (entity.equals(currentEntity)) {
//			System.out.println("... reusing bean for entity: " + currentEntity);
		}
		else {
			currentEntity = entity;
			switch (currentEntity) {
				case "address":
					currentBean = (BackingBean<Address>) addressBean;
					break;
				case "student":
					currentBean = studentBean;
					break;
				case "teacher":
					currentBean = teacherBean;
					break;
				case "course":
					currentBean = courseBean;
					break;
				default:
					// not allowed to return null from a non-dependent producer method
					currentBean = homeBean;
			}
		}
		currentBean.setAction(action);
		System.out.printf("... bean produced: '%s' for action '%s'%n", currentBean.getEntity(), action);
		return currentBean;
	}
}
