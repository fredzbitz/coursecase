/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfred.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author fred
 */
public class Parameters {
	final static String FIELD_START = ".";
	private Map<String, Object> parameterMap;
	
	public Parameters(HttpServletRequest request) {
		Map<String, String[]> requestMap = request.getParameterMap();
		
		// old SE 7 : HOW (imperative programming)
		Set<String> keySet = requestMap.keySet();
		Map<String, String[]> newMap = new HashMap<>();
		for (String key: keySet) {
			if (key.startsWith(FIELD_START)) {
				String[] value = requestMap.get(key);
				key = key.substring(1);
				newMap.put(key, value);
			}
		}
		
		// new SE 8 : WHAT (declarative programming)
		parameterMap = requestMap.keySet().parallelStream()		// for
				.filter(key -> key.startsWith(FIELD_START))		// if
				.map(key -> key.substring(1))					// substring
				.collect(Collectors.toMap(key -> key, key -> valueForKey(requestMap, key)));	// put
		System.out.println("... parameters: " + parameterMap);
	}
	
	private static Object valueForKey(Map<String, String[]> requestMap, String key) {
		String[] value = requestMap.get(FIELD_START + key);
		switch (value.length) {
			case 0:		return null;
			case 1:		return value[0];
			default:	return value;
		}
	}
	
	public void put(String key, Object value) {
		parameterMap.put(key, value);
	}
	
	public Object get(String key) {
		return parameterMap.get(key);
	}
	
	public String getString(String key) {
		return (String) get(key);
	}
	
	public int getInt(String key) {
		return (Integer) get(key);
	}
	
	public long getLong(String key) {
		return (Long) get(key);
	}
	
	// etc

	@Override
	public String toString() {
		return parameterMap.toString();
	}
}
