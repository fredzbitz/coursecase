/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfred.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 *
 * @author fred
 */
public class Messages {

	private Map<String, String> messageMap;

	public Messages() {
		messageMap = new HashMap<>();
	}

	public int size() {
		return messageMap.size();
	}

	public void add(String field, String messageKey) {
		messageMap.put(field.toLowerCase(), messageKey);
	}

	public String get(String key) {
		// get localized message from bundle
		String messageKey = messageMap.get(key.toLowerCase());
		if (messageKey != null) {
			// fist, try app bundle
			ResourceBundle bundle = ResourceBundle.getBundle("view.messages");
			String message = get(messageKey, bundle);
			if (message == null) {
				// next, try framework bundle
				bundle = ResourceBundle.getBundle("jsfred.view.messages");
				message = get(messageKey, bundle);
			}
			if (message != null) {
				return message;
			}
			else {
				// otherwise just return the non-localized message
				System.out.println("!!! bundle key not found: " + messageKey);
				return messageKey;
			}
		}
		return null;
	}

	private String get(String key, ResourceBundle bundle) {
		try {
			// try to localize
			return bundle.getString(key);
		}
		catch (MissingResourceException mrx) {
			// key not found
			return null;
		}
	}

	@Override
	public String toString() {
		return messageMap.toString();
	}

}
