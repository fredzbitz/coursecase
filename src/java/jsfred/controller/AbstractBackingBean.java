/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfred.controller;

import jsfred.model.Record;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.ejb.EJBException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import jsfred.model.AbstractFacade;
import static jsfred.view.ActionType.LIST;

/**
 *
 * @author fred
 */
public abstract class AbstractBackingBean<T extends Record> implements BackingBean<T>, Serializable {

	private static final long serialVersionUID = 1L;
	private Parameters parameters;
	private Query query;
	private T selectedRecord;
	private String action;

	@Override
	public void setRequest(HttpServletRequest request) {
		System.out.println("... setRequest: " + request.getPathInfo());
		parameters = new Parameters(request);
		query = new Query(request);
		selectedRecord = null;
		String id = request.getMethod().equalsIgnoreCase("GET") ? query.get("id") : parameters.getString("id");
		if (id != null) {
			try {
				int idInt = Integer.parseInt(id);
				selectedRecord = getRecord(idInt);
			}
			catch (NumberFormatException x) {
				// nothing
			}
		}
	}

	@Override
	public Parameters getParameters() {
		return parameters;
	}

	@Override
	public Query getQuery() {
		return query;
	}

	@Override
	public String getEntity() {
		String name = getClass().getSimpleName();
		int index = name.indexOf("Bean");
		name = name.substring(0, index).toLowerCase();
		return name;
	}

	@Override
	public void setAction(String action) {
		this.action = action;
	}

	@Override
	public String getAction() {
		return action;
	}

	public abstract T newRecord();

	@Override	// until JSP recognizes the default method inherited from the BackingBean interface
	public List<T> getRecords() {
		return BackingBean.super.getRecords();
	}

	@Override	// until JSP recognizes the default method inherited from the BackingBean interface
	public T getRecord(long id) {
		return (T) BackingBean.super.getRecord(id);
	}

	@Override
	public T getSelectedRecord() {
		return selectedRecord;
	}

	public void setSelectedRecord(T record) {
		selectedRecord = record;
	}

	// OBSOLETE - now handled by bean validation
	protected void require(String field, Messages messages) {
		String parameter = getParameters().getString(field);
		if (parameter == null || parameter.length() == 0) {
			messages.add(field, "FIELD_REQUIRED");
		}
	}

	public void convertToDate(String field, Messages messages) {
		// convert date
		String dateString = getParameters().getString(field);
		if (dateString != null && dateString.length() > 0) {
			DateFormat fmt = new SimpleDateFormat("dd MMM yyyy");
			try {
				Date date = fmt.parse(dateString);
				getParameters().put(field, date);
			}
			catch (ParseException ex) {
				System.out.println("!!! parse error: " + dateString);
				messages.add(field, "DATE_ILLEGAL_FORMAT");
			}
		}
		else {
			getParameters().put(field, null);
		}
	}

	public <T extends Record> void convertToRecord(String field, AbstractFacade<T> f, Messages messages) {
		String idString = getParameters().getString(field);
		if (idString != null && idString.length() > 0) {
			Long id = Long.valueOf(idString);
			T record = f.find(id);
			getParameters().put(field, record);
		}
		else {
			getParameters().put(field, null);
		}
	}

	public void convertToInteger(String field, Messages messages) {
		String integerString = getParameters().getString(field);
		if (integerString != null && integerString.length() > 0) {
			try {
				Integer integer = Integer.parseInt(integerString);
				getParameters().put(field, integer);
			}
			catch (NumberFormatException ex) {
				System.out.println("!!! parse error: " + integerString);
				messages.add(field, "INTEGER_ILLEGAL_FORMAT");
			}
		}
		else {
			getParameters().put(field, null);
		}
	}

	public void convertToLong(String field, Messages messages) {
		String longString = getParameters().getString(field);
		if (longString != null && longString.length() > 0) {
			try {
				Long longValue = Long.parseLong(longString);
				getParameters().put(field, longValue);
			}
			catch (NumberFormatException ex) {
				System.out.println("!!! parse error: " + longString);
				messages.add(field, "INTEGER_ILLEGAL_FORMAT");
			}
		}
		else {
			getParameters().put(field, null);
		}
	}

	public void validate(Record record, Messages messages) {
		// bean validation
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Record>> violations = validator.validate(record);
		for (ConstraintViolation<Record> violation : violations) {
			System.out.println("!!! constraint violation: " + violation);
			String key = violation.getPropertyPath().toString();
			if (key.length() == 0) {
				key = violation.getRootBeanClass().getSimpleName();
			}
			messages.add(key, violation.getMessage());
		}
	}

	@Override
	public void validate(Messages messages) throws ValidationException {
		// get record to validate
//		Long id = getQuery().get("id") != null ? Long.valueOf(getQuery().get("id")) : null;
		T record = getSelectedRecord();
		switch (getAction()) {
			case "edit":
				if (record == null) {	// add
					record = newRecord();
					record.setFields(getParameters());
				}
				else {				// update
					record = getCourseFacade().find(record.getId());
					record.setFields(getParameters());
				}
				break;
			default:
				return;				// no validation required for other actions
		}
		// bean validation
		validate(record, messages);
	}

	@Override
	public String execute(Messages messages) throws ExecutionException {
		System.out.printf("..x %s action: %s (query: %s)%n", getEntity(), getAction(), getQuery());
//		Long id = getQuery().get("id") != null ? Long.valueOf(getQuery().get("id")) : null;
		T record = getSelectedRecord();
		String action = getAction();

		// give subclasses a chance to handle the action
		String result = action(action, messages);
		if (result != null) {
			// throw exception if any execution errors occurred
			if (messages.size() != 0) {
				throw new ExecutionException(messages, getAction());
			}
			return result;
		}

		// else perform standard behaviour
		try {
			switch (action) {
				case "edit":
					if (record == null) {	// add
						record = newRecord();
						record.setFields(getParameters());
						getCourseFacade().create(record);
					}
					else {				// update
						record = getCourseFacade().find(record.getId());
						record.setFields(getParameters());
						getCourseFacade().edit(record);
					}
					break;
				case "delete":
					getCourseFacade().remove(record);
					break;
			}
			return LIST.pageForEntity(getEntity());
		}
		catch (EJBException e) {
//			Throwable t = e.getCause();
//			messages.add(getEntity(), t == null ? e.getMessage() : t.getMessage());
			messages.add(getEntity(), String.format("Action failed: %s", getAction()));
			throw new ExecutionException(messages, getAction());
		}
	}

	@Override
	public String cancel() {
		return LIST.pageForEntity(getEntity());
	}

	protected String action(String action, Messages messages) {
		return null;
	}

}
