/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfred.controller;

import jsfred.model.Record;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import jsfred.model.AbstractFacade;

/**
 *
 * @author fred
 */
public interface BackingBean <T extends Record> {
	// request analysis
	public void setRequest(HttpServletRequest request);
	public Parameters getParameters();
	public Query getQuery();
	public String getEntity();
	public void setAction(String action);
	public String getAction();

	// parameter handling
	public void convert(Messages messages) throws ConversionException;
	public void validate(Messages messages) throws ValidationException;
	
	// form cancelled
	public String cancel();
	
	// action 
	public String execute(Messages messages) throws ExecutionException;
	
	public AbstractFacade<T> getCourseFacade();
	
	public T getSelectedRecord();
	
	// the bad news: default methods are not recognized by JSP...
	// so, every implementing class must override them
	public default List<T> getRecords() {
		return getCourseFacade().findAll();
	}

	public default T getRecord(long id) {
		return getCourseFacade().find(id);
	}
	
	public static class Exception extends java.lang.Exception {
		private Messages messages;
		private String page;
		
		public Exception(String message, Messages messages, String page) {
			super(message);
			this.messages = messages;
			this.page = page;
		}
		
		public Messages getMessages() {
			return messages;
		}

		public String getPage() {
			return page;
		}

		@Override
		public String toString() {
			return String.format("%s: %s", getMessage(), messages);
		}
		
		
	}
	
	public static class ConversionException extends Exception {
		public ConversionException(Messages messages, String page) {
			super("Conversion error", messages, page);
		}
	}
	
	public static class ValidationException extends Exception {
		public ValidationException(Messages messages, String page) {
			super("Validation error", messages, page);
		}
	}
	
	public static class ExecutionException extends Exception {
		public ExecutionException(Messages messages, String page) {
			super("Execution error", messages, page);
		}
	}
}
