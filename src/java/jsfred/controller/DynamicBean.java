/*
 * The Qualifier Annotation for producing dynamic beans that will be injected.
 */
package jsfred.controller;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.inject.Qualifier;

/**
 *
 * @author fred
 */
@Qualifier
@Retention(RUNTIME)
@Target({METHOD, FIELD})
public @interface DynamicBean {
	
}
