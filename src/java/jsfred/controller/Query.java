/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfred.controller;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author fred
 */
public class Query {

	private Map<String, String> queryMap;

	public Query(HttpServletRequest request) {
		queryMap = new HashMap<>();
		String queryString = request.getQueryString();
		// query format: key=value&key=value&etc
		// where "=value" is optional
		if (queryString != null) {
			// split into an array of key[=value] pairs
			for (String pair: queryString.split("&")) {
				int equalsIndex = pair.indexOf("=");
				if (equalsIndex < 0) {
					// key only
					put(pair, "");
				}
				else {
					// key=value
					String key = pair.substring(0, equalsIndex);
					String value = pair.substring(equalsIndex + 1);
					put(key, value);
				}
			}
			System.out.println("... query: " + queryMap);
		}
	}

	private void put(String key, String value) {
		queryMap.put(key, value);
	}

	public String get(String key) {
		return queryMap.get(key);
	}
	
	public int getInt(String key) {
		return Integer.valueOf(get(key));
	}

	@Override
	public String toString() {
		return queryMap.toString();
	}
}
