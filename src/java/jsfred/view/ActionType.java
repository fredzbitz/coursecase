/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfred.view;

/**
 *
 * @author fred
 */
public enum ActionType {
	DEFAULT {
		// produce page name "/ENTITY"
		@Override
		public String pageForEntity(String entity) {
			return String.format("/%s", entity);
		}

	},
	LIST, EDIT, ADD, DELETE, SHOW;

	// produce page name "/ENTITY/ACTION"
	public String pageForEntity(String entity) {
		return String.format("/%s/%s", entity, this.toString().toLowerCase());
	}
}
