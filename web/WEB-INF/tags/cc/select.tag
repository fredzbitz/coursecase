<%-- 
    Document   : input
    Created on : Dec 4, 2016, 4:52:34 PM
    Author     : fred
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="record" required="true" type="jsfred.model.Record"%>
<%@attribute name="field" required="true"%>
<%@attribute name="items" required="true" type="java.util.List<jsfred.model.Record>"%>

<%-- any content can be specified here e.g.: --%>
<div class="form-group">
	<c:set var="name" value=".${field}"/>
	<label class="control-label col-sm-2" for="${field}"><jsp:doBody/></label>
	<div class="col-sm-10">
		<select name="${name}" class="form-control" id="${field}">
			<c:forEach var="item" items="${items}">
				<option value="${item.id}" ${item.id == record[field].id ? 'selected' : ''}>${item}</option>
			</c:forEach>
		</select>
		<span class="error">${messages.get(field)}</span>
	</div>
</div>
