<%-- 
    Document   : mainLayout
    Created on : Nov 26, 2016, 6:01:31 PM
    Author     : fred
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="cc" tagdir="/WEB-INF/tags/cc" %>
<%@tag description="main page layout" pageEncoding="UTF-8"%>

<%-- list of normal or fragment attributes: --%>
<%@attribute name="header" fragment="true"%>
<%@attribute name="footer" fragment="true"%>
<%@attribute name="user"%>

<%-- content: --%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Course Case</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="/cc/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <style>
            body {
                padding: 50px 20px 20px 20px;
            }
        </style>
        <link rel="stylesheet" href="/cc/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="/cc/css/main.css">

        <script src="/cc/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
	</head>
	<body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<cc:page entity="home"					/>">Home</a>
					<a class="navbar-brand" href="<cc:page entity="address" action="list"	/>">Address</a>
					<a class="navbar-brand" href="<cc:page entity="student" action="list"	/>">Student</a>
					<a class="navbar-brand" href="<cc:page entity="teacher" action="list"	/>">Teacher</a>
					<a class="navbar-brand" href="<cc:page entity="course"  action="list"	/>">Course</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<c:if test="${empty user}">
						<form class="navbar-form navbar-right" role="form">
							<div class="form-group">
								<input type="text" placeholder="Email" class="form-control">
							</div>
							<div class="form-group">
								<input type="password" placeholder="Password" class="form-control">
							</div>
							<button type="submit" class="btn btn-success">Sign in</button>
						</form>
					</c:if>
					<c:if test="${not empty user}">
						<form class="navbar-form navbar-right" role="form">
							<div class="form-group">
								<span class="text-primary">current user: ${user}&nbsp;</span>
							</div>
							<button type="submit" class="btn btn-success">Sign out</button>
						</form>
					</c:if>
				</div><!--/.navbar-collapse -->
			</div>
		</nav>
		<div id="pageheader">
			<jsp:invoke fragment="header"/>
		</div>
		<div id="body">
			<jsp:doBody/>
		</div>
		<div id="pagefooter">
			<br/><br/>
			<p id="copyright">
				<jsp:invoke fragment="footer"/>
				- Course Case &COPY; 2016, Fred Zuijdendorp
			</p>
		</div>
	</body>
</html>
