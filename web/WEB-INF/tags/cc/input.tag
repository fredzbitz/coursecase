<%-- 
    Document   : input
    Created on : Dec 4, 2016, 4:52:34 PM
    Author     : fred
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="record" required="true" type="jsfred.model.Record"%>
<%@attribute name="field" required="true"%>
<%@attribute name="datePattern"%>
<%@attribute name="placeholder"%>

<%-- any content can be specified here e.g.: --%>
<div class="form-group">
	<c:set var="name" value=".${field}"/>
	<c:set var="value" value="${not empty record ? record[field] : param[name]}"/>
	<c:if test="${not empty datePattern && not empty record}">
		<fmt:formatDate value="${value}" pattern="${datePattern}" var="value"/>
	</c:if>
	<label class="control-label col-sm-2" for="${field}"><jsp:doBody/></label>
	<div class="col-sm-10">
		<input class="form-control" id="${field}" placeholder="${placeholder}" name="${name}" value="${value}">
		<span class="error">${messages.get(field)}</span>
	</div>
</div>
	