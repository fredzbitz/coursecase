<%-- 
    Document   : delete
    Created on : Dec 27, 2016, 9:45:01 PM
    Author     : fred
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="cc" tagdir="/WEB-INF/tags/cc" %>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="entity" required="true"%>
<%@attribute name="id" required="true"%>
<%@attribute name="ref"%>
<%@attribute name="ref_id"%>
<%@attribute name="from"%>

<%-- any content can be specified here e.g.: --%>
<c:set var="action" value="delete"/>
<c:if test="${not empty ref}">
	<c:set var="action" value="${action}_${ref}"/>
</c:if>
<form method="post" action="<cc:action page="${entity}/${action}"/>">
	<button type="submit"><span class="glyphicon glyphicon-minus"/></button>
	<input type="hidden" name=".id" value="${id}" />
	<input type="hidden" name=".ref_id" value="${ref_id}" />
	<input type="hidden" name=".from" value="${from}" />
</form>
