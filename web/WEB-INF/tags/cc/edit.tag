<%-- 
    Document   : edit
    Created on : Dec 27, 2016, 9:35:33 PM
    Author     : fred
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@taglib prefix="cc" tagdir="/WEB-INF/tags/cc" %>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="entity" required="true"%>
<%@attribute name="id" required="true"%>

<%-- any content can be specified here e.g.: --%>
<form method="get" action="<cc:action page="${entity}/edit"/>">
	<button type="submit"><span class="glyphicon glyphicon-pencil"/></button>
	<input type="hidden" name="id" value="${id}" />
</form>
