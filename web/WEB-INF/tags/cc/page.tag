<%-- 
    Document   : page
    Created on : Nov 30, 2016, 4:07:00 PM
    Author     : fred
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="entity" required="true"%>
<%@attribute name="action" required="false"%>

<%-- any content can be specified here e.g.: --%>
/cc/page/${entity}/${action}