<%-- 
    Document   : link
    Created on : Nov 30, 2016, 3:47:11 PM
    Author     : fred
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="entity" required="true"%>
<%@attribute name="action" required="true"%>

<%-- any content can be specified here e.g.: --%>
<a href="/cc/page/${entity}/${action}"><jsp:doBody/></a>