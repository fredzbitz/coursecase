<%-- 
    Document   : add
    Created on : Dec 27, 2016, 9:44:19 PM
    Author     : fred
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@taglib prefix="cc" tagdir="/WEB-INF/tags/cc" %>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="entity" required="true"%>

<%-- any content can be specified here e.g.: --%>
<form method="get" action="<cc:action page="${entity}/edit"/>">
	<button type="submit"><span class="glyphicon glyphicon-plus"/></button>
</form>
