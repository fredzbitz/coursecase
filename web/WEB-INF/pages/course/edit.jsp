<%-- 
    Document   : course/edit
    Created on : Nov 29, 2016, 1:30:18 PM
    Author     : fred
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="cc" tagdir="/WEB-INF/tags/cc" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<cc:mainLayout user="Fred">
    <jsp:attribute name="header">
		<h1>Course</h1>
    </jsp:attribute>

    <jsp:attribute name="footer">
		course edit
    </jsp:attribute>

    <jsp:body>
		<c:set var="course" value="${courseBean.selectedRecord}"/>
		<c:if test="${not empty course}">
			<c:set var="buttonTitle" value="Update"/>
		</c:if>
		<c:if test="${empty course}">
			<c:set var="buttonTitle" value="Add"/>
		</c:if>
		<div class="container">
			<form class="form-horizontal" method="post" action="<cc:action page="course/edit"/>">
				<input type="hidden" name=".id" value="${course.id}" />
				<input type="hidden" name=".from" value="edit" />

				<cc:input record="${course}" field="title" placeholder="Enter title">Title:</cc:input>
				<cc:select record="${course}" field="teacher" items="${teacherBean.records}">Teacher:</cc:select>
				<cc:input record="${course}" field="startDate" datePattern="dd MMM yyyy" placeholder="Enter start date">Start date:</cc:input>
				<cc:input record="${course}" field="durationDays" placeholder="Enter duration in days">Duration (days):</cc:input>

				<!--displaying the class level validation errors here-->
				<!--TODO: should be a list! (it's only one error now - the last one added to the messages wins)-->
				<div class="form-group">
					<label class="control-label col-sm-2" ></label>
					<div class="col-sm-10">
						<span class="error">${messages.get('course')}</span>
					</div>
				</div>

				<div class="form-group"> 
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default" name="ok">${buttonTitle}</button>
						&nbsp;
						<button type="submit" class="btn btn-default" name="cancel">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</jsp:body>
</cc:mainLayout>
