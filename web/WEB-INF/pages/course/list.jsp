<%-- 
    Document   : course/list
    Created on : Nov 26, 2016, 6:06:54 PM
    Author     : fred
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="cc" tagdir="/WEB-INF/tags/cc" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<cc:mainLayout>
    <jsp:attribute name="header">
      <h1>Course</h1>
    </jsp:attribute>
	  
    <jsp:attribute name="footer">
		course list
    </jsp:attribute>
		
    <jsp:body>
		<div class="container">
			<h2>Some title concerning courses</h2>
			<p><span class="error">${messages.get("course")}</span></p>            
			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th>title</th>
						<th>teacher</th>
						<th>students</th>
						<th>start date</th>
						<th>duration (days)</th>
					</tr>
				</thead>	
				<tbody>
					<c:if test="${empty courseBean.records}">
						<p>no courses found</p>
					</c:if>
					<c:forEach var="course" items="${courseBean.records}">
						<tr>
							<td>${course.title}</td>
							<td>${course.teacher}</td>
							<td>
								${course.students.size()}
								&nbsp;(<a href="<cc:action page="course/show?id=${course.id}"/>">edit</a>)
							</td>
							<td><fmt:formatDate value="${course.startDate}" pattern="dd MMM yyyy"/></td>
							<td>${course.durationDays}</td>
							<td><cc:edit entity="course" id="${course.id}"/></td>
							<td><cc:delete entity="course" from="list" id="${course.id}"/></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<cc:add entity="course"/>
		</div>
    </jsp:body>
</cc:mainLayout>
