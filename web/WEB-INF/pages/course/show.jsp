<%-- 
    Document   : show
    Created on : Dec 28, 2016, 9:38:55 AM
    Author     : fred
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="cc" tagdir="/WEB-INF/tags/cc" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<cc:mainLayout user="Fred">
    <jsp:attribute name="header">
		<h1>Course</h1>
    </jsp:attribute>

    <jsp:attribute name="footer">
		course show
    </jsp:attribute>

    <jsp:body>
		<c:set var="course" value="${courseBean.selectedRecord}"/>
		<div class="container">
			<form class="form-horizontal">
				<cc:output record="${course}" field="title">Title:</cc:output>
				<cc:output record="${course}" field="teacher">Teacher:</cc:output>
				<cc:output record="${course}" field="startDate" datePattern="dd MMM yyyy">Start date:</cc:output>
				<cc:output record="${course}" field="durationDays">Duration (days):</cc:output>
				</form>

				<h3>&nbsp;</h3>
				<h3>Students in course:</h3>            
			<c:if test="${empty course.students}" var="noStudents">
				<h5>no students in course</h5>
			</c:if>
			<c:if test="${not noStudents}">
				<h4><span class="error">${messages.get("delete_student")}</span></h4>            
				<table class="table table-condensed table-striped">
					<thead>
						<tr>
							<th>first name</th>
							<th>last name</th>
							<th>address</th>
							<th>birth date</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="student" items="${course.students}">
							<tr>
								<td>${student.firstName}</td>
								<td>${student.lastName}</td>
								<td>${student.address}</td>
								<td><fmt:formatDate value="${student.birthDate}" pattern="dd MMM yyyy"/></td>
								<td><cc:delete entity="course" from="show" id="${course.id}" ref="student" ref_id="${student.id}"/></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</c:if>

			<h3>&nbsp;</h3>
			<h3>Available students:</h3>            
			<div class="form-group">
				<div class="col-sm-6">
					<input class="form-control" id="search" placeholder="search" name=".search" onkeyup="doSearch();">
					<span class="error">${messages.get(field)}</span>
				</div>
			</div>
			<br/><br/>
			<h4><span class="error">${messages.get("add_student")}</span></h4>            
			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th>first name</th>
						<th>last name</th>
						<th>address</th>
						<th>birth date</th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${empty courseBean.availableStudents}">
					<p>no students available</p>
				</c:if>
				<c:forEach var="student" items="${courseBean.availableStudents}">
					<tr>
						<td>${student.firstName}</td>
						<td>${student.lastName}</td>
						<td>${student.address}</td>
						<td><fmt:formatDate value="${student.birthDate}" pattern="dd MMM yyyy"/></td>
						<td><cc:addTo entity="course" from="show" id="${course.id}" ref="student" ref_id="${student.id}"/></td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
	</jsp:body>
</cc:mainLayout>
