<%-- 
    Document   : address
    Created on : Nov 26, 2016, 6:06:54 PM
    Author     : fred
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="cc" tagdir="/WEB-INF/tags/cc" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<cc:mainLayout>
    <jsp:attribute name="header">
		<h1>Address</h1>
    </jsp:attribute>

    <jsp:attribute name="footer">
		address list
    </jsp:attribute>

    <jsp:body>
		<div class="container">
			<h2>Some title</h2>
			<p><span class="error">${messages.get("address")}</span></p>            
			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th>street</th>
						<th>number</th>
						<th>zip</th>
						<th>city</th>
						<th>country</th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${empty addressBean.records}">
						<p>no addresses found</p>
					</c:if>
					<c:forEach var="address" items="${addressBean.records}">
						<tr>
							<td>${address.street}</td>
							<td>${address.number}</td>
							<td>${address.zip}</td>
							<td>${address.city}</td>
							<td>${address.country}</td>
							<td><cc:edit entity="address" id="${address.id}"/></td>
							<td><cc:delete entity="address" from="list" id="${address.id}"/></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<cc:add entity="address"/>
		</div>
    </jsp:body>
</cc:mainLayout>
