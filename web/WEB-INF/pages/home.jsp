<%-- 
    Document   : home
    Created on : Nov 29, 2016, 1:02:17 PM
    Author     : fred
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="cc" tagdir="/WEB-INF/tags/cc" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<cc:mainLayout user="Fred">
    <jsp:attribute name="header">
		<h1>Home</h1>
    </jsp:attribute>

    <jsp:attribute name="footer">
		home
    </jsp:attribute>

    <jsp:body>
		<div class="container">
			<h2>Welcome home</h2>
			<p>This is just a starter.</p>            
			<form method="post" action="<cc:action page="home/init"/>">
				<!--due to a bug in delete with jpa the delete button is disabled-->
				<button type="submit">init database</button>
			</form>
		</div>
    </jsp:body>
</cc:mainLayout>
