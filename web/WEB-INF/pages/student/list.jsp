<%-- 
    Document   : student/list
    Created on : Nov 26, 2016, 6:06:54 PM
    Author     : fred
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="cc" tagdir="/WEB-INF/tags/cc" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<cc:mainLayout user="Fred">
    <jsp:attribute name="header">
      <h1>Student</h1>
    </jsp:attribute>
	  
    <jsp:attribute name="footer">
		student list
    </jsp:attribute>
		
    <jsp:body>
		<div class="container">
			<h2>Some title concerning students</h2>
			<p><span class="error">${messages.get("student")}</span></p>            
			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th>first name</th>
						<th>last name</th>
						<th>courses</th>
						<th>address</th>
						<th>birth date</th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${empty studentBean.records}">
						<p>no students found</p>
					</c:if>
					<c:forEach var="student" items="${studentBean.records}">
						<tr>
							<td>${student.firstName}</td>
							<td>${student.lastName}</td>
							<td>${student.courses.size()}</td>
							<td>${student.address}</td>
							<td><fmt:formatDate value="${student.birthDate}" pattern="dd MMM yyyy"/></td>
							<td><cc:edit entity="student" id="${student.id}"/></td>
							<td><cc:delete entity="student" from="list" id="${student.id}"/></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<cc:add entity="student"/>
		</div>
    </jsp:body>
</cc:mainLayout>
