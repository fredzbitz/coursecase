<%-- 
    Document   : teacher/list
    Created on : Nov 26, 2016, 6:06:54 PM
    Author     : fred
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="cc" tagdir="/WEB-INF/tags/cc" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<cc:mainLayout>
    <jsp:attribute name="header">
      <h1>Teacher</h1>
    </jsp:attribute>
	  
    <jsp:attribute name="footer">
		teacher list
    </jsp:attribute>
		
    <jsp:body>
		<div class="container">
			<h2>Some title concerning teachers</h2>
			<p><span class="error">${messages.get("teacher")}</span></p>            
			<table class="table table-condensed table-striped">
				<thead>
					<tr>
						<th>first name</th>
						<th>last name</th>
						<th>courses</th>
						<th>address</th>
						<th>birth date</th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${empty teacherBean.records}">
						<p>no teachers found</p>
					</c:if>
					<c:forEach var="teacher" items="${teacherBean.records}">
						<tr>
							<td>${teacher.firstName}</td>
							<td>${teacher.lastName}</td>
							<td>${teacher.courses.size()}</td>
							<td>${teacher.address}</td>
							<td><fmt:formatDate value="${teacher.birthDate}" pattern="dd MMM yyyy"/></td>
							<td><cc:edit entity="teacher" id="${teacher.id}"/></td>
							<td><cc:delete entity="teacher" from="list" id="${teacher.id}"/></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<cc:add entity="teacher"/>
		</div>
    </jsp:body>
</cc:mainLayout>
