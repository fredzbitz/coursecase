<%-- 
    Document   : edit
    Created on : Nov 29, 2016, 1:30:18 PM
    Author     : fred
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="cc" tagdir="/WEB-INF/tags/cc" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<cc:mainLayout user="Fred">
    <jsp:attribute name="header">
		<h1>Teacher</h1>
    </jsp:attribute>

    <jsp:attribute name="footer">
		teacher edit
    </jsp:attribute>

    <jsp:body>
		<c:set var="teacher" value="${teacherBean.selectedRecord}"/>
		<c:if test="${not empty teacher}">
			<c:set var="buttonTitle" value="Update"/>
		</c:if>
		<c:if test="${empty teacher}">
			<c:set var="buttonTitle" value="Add"/>
		</c:if>
		<div class="container">
			<form class="form-horizontal" method="post" action="<cc:action page="teacher/edit"/>">
				<input type="hidden" name=".id" value="${teacher.id}" />
				<input type="hidden" name=".from" value="edit" />

				<cc:input record="${teacher}" field="firstName" placeholder="Enter first name">First name:</cc:input>
				<cc:input record="${teacher}" field="lastName" placeholder="Enter last name">Last name:</cc:input>
				<cc:select record="${teacher}" field="address" items="${addressBean.records}">Address:</cc:select>
				<cc:input record="${teacher}" field="birthDate" datePattern="dd MMM yyyy" placeholder="Enter birth date">Birth date:</cc:input>

				<!--displaying the class level validation errors here-->
				<!--TODO: should be a list! (it's only one error now - the last one added to the messages wins)-->
				<div class="form-group">
					<label class="control-label col-sm-2" ></label>
					<div class="col-sm-10">
						<span class="error">${messages.get('teacher')}</span>
					</div>
				</div>

				<div class="form-group"> 
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default" name="ok">${buttonTitle}</button>
						&nbsp;
						<button type="submit" class="btn btn-default" name="cancel">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</jsp:body>
</cc:mainLayout>
